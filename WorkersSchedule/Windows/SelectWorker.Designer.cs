﻿
namespace GrafikSklepowy.Windows
{
    partial class SelectWorker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.workersGrid = new System.Windows.Forms.DataGridView();
            this.workerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerSurname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.workersGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // workersGrid
            // 
            this.workersGrid.AllowUserToAddRows = false;
            this.workersGrid.AllowUserToDeleteRows = false;
            this.workersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.workersGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.workerName,
            this.workerSurname});
            this.workersGrid.Location = new System.Drawing.Point(12, 12);
            this.workersGrid.MultiSelect = false;
            this.workersGrid.Name = "workersGrid";
            this.workersGrid.RowTemplate.Height = 25;
            this.workersGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.workersGrid.Size = new System.Drawing.Size(432, 426);
            this.workersGrid.TabIndex = 3;
            this.workersGrid.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.workersGrid_RowHeaderMouseClick);
            // 
            // workerName
            // 
            this.workerName.HeaderText = "Name";
            this.workerName.Name = "workerName";
            // 
            // workerSurname
            // 
            this.workerSurname.HeaderText = "Surname";
            this.workerSurname.Name = "workerSurname";
            // 
            // SelectWorker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 450);
            this.Controls.Add(this.workersGrid);
            this.Name = "SelectWorker";
            this.Text = "Select Worker";
            ((System.ComponentModel.ISupportInitialize)(this.workersGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView workersGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerSurname;
    }
}