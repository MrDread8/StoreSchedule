﻿using System;

namespace GrafikSklepowy
{
    public class PlannedDay
    {
        private DateTime date;
        private int workerId;
        private decimal plannedHours;
        private decimal workedHours = 0;

        public PlannedDay(DateTime date, int workerID, decimal plannedHours)
        {
            this.date = date.Date;
            this.workerId = workerID;
            this.plannedHours = plannedHours;
        }
        public PlannedDay()
        {

        }
        public DateTime Date
        {
            get => this.date;
            set => this.date = (DateTime)value;
        }
        public int WorkerID
        {
            get => this.workerId;
            set => this.workerId = (int)value;
        }
        public decimal PlannedHours
        {
            get => this.plannedHours;
            set => this.plannedHours = (decimal)value;
        }
        public decimal WorkHours
        {
            get => this.workedHours;
            set => this.workedHours = (decimal)value;
        }
    }
}
