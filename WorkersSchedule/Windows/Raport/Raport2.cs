﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using GrafikSklepowy.Class;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrafikSklepowy.Windows.Raport
{
    public partial class Raport2 : Form
    {
        private int workerID;
        public Raport2(int id)
        {
            InitializeComponent();
            for(int i =0;i < PlannedDayList.Count; i++)
            {
                if(PlannedDayList.GetPlannedDay(i).PlannedHours != PlannedDayList.GetPlannedDay(i).WorkHours && PlannedDayList.GetPlannedDay(i).WorkerID == workerID)
                {
                    raport2Grid.Rows.Add(PlannedDayList.GetPlannedDay(i).Date, PlannedDayList.GetPlannedDay(i).PlannedHours, PlannedDayList.GetPlannedDay(i).WorkHours);
                }
            }
        }

    }
}
