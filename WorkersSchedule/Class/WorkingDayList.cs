﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace GrafikSklepowy.Class
{
    public static class WorkingDayList
    {
        private static List<WorkingDay> workingDays = new();

        public static void AppendWorkingDay(WorkingDay w)
        {
            workingDays.Add(w);
        }
        public static WorkingDay GetWorkingDay(int i)
        {
            return workingDays[i];
        }
        public static void SetWorkingDay(int i, WorkingDay w)
        {
            workingDays[i] = w;
        }
        public static void RemoveEntry(int id)
        {
            workingDays.RemoveAt(id);
        }
        public static int Count
        {
            get => workingDays.Count();
        }
        public static int FindDate(DateTime date)
        {
            for (int i = 0; i < Count; i++)
            {
                if (workingDays[i].Date == date.Date)
                    return i;
            }
            return -1;
        }
        public static void Save()
        {
            XmlSerializer serializer = new XmlSerializer(workingDays.GetType());
            using (TextWriter filestream = new StreamWriter("WorkingDays.xml"))
            {
                serializer.Serialize(filestream, workingDays);
            }
        }
        public static void Load()
        {
            XmlSerializer deserialize = new XmlSerializer(workingDays.GetType());
            if (File.Exists("WorkingDays.xml"))
            {
                using (TextReader filestream = new StreamReader("WorkingDays.xml"))
                {
                    workingDays = (List<WorkingDay>)deserialize.Deserialize(filestream);
                }
            }
        }
    }
}
