﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace GrafikSklepowy.Class
{
    public static class WorkerList
    {
        public static List<Worker> workers = new List<Worker>();
        public static void AppendWorker(Worker w)
        {
            workers.Add(w);
        }
        public static Worker GetWorker(int i)
        {
            return workers[i];
        }
        public static int Count
        {
            get => workers.Count();
        }
        public static void SetWorker(int i, Worker w)
        {
            workers[i] = w;
        }
        public static void CalculateSalary()
        {
            for (int i = 0; i < Count; i++)
            {
                decimal tmp = 0;
                for (int j = 0; j < PlannedDayList.Count; j++)
                {
                    if (PlannedDayList.GetPlannedDay(i).Date >= DateTime.Today.AddDays(-30).Date && PlannedDayList.GetPlannedDay(j).Date < DateTime.Today.Date && PlannedDayList.GetPlannedDay(j).WorkerID == i)
                    {
                        tmp = (PlannedDayList.GetPlannedDay(j).WorkHours - PlannedDayList.GetPlannedDay(j).PlannedHours) * 3;
                        tmp += PlannedDayList.GetPlannedDay(j).WorkHours;
                        tmp *= workers[i].HourlyRate;
                    }
                }
                workers[i].Salary = tmp;
            }
        }
        public static void Save()
        {
            XmlSerializer serializer = new XmlSerializer(workers.GetType());
            using (TextWriter filestream = new StreamWriter("Workers.xml"))
            {
                serializer.Serialize(filestream, workers);
            }
        }
        public static void Load()
        {
            XmlSerializer deserialize = new XmlSerializer(workers.GetType());
            if (File.Exists("Workers.xml"))
            {
                using (TextReader filestream = new StreamReader("Workers.xml"))
                {
                    workers = (List<Worker>)deserialize.Deserialize(filestream);
                }
            }
        }
    }
}
