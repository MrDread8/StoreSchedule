﻿using GrafikSklepowy.Class;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace GrafikSklepowy.Windows
{
    public partial class ClearingWindow : Form
    {
        private int plannedDayID;
        public ClearingWindow(int id)
        {
            InitializeComponent();
            plannedDayID = id;
            WorkerData.Text = WorkerList.GetWorker(PlannedDayList.GetPlannedDay(id).WorkerID).Name + " " + WorkerList.GetWorker(PlannedDayList.GetPlannedDay(id).WorkerID).Surname;
            PlannedDate.Text = PlannedDayList.GetPlannedDay(id).Date.ToString();
            PlannedHours.Text = PlannedDayList.GetPlannedDay(id).PlannedHours.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PlannedDayList.GetPlannedDay(plannedDayID).WorkHours = FinishedHours.Value;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FinishedHours_ValueChanged(object sender, EventArgs e)
        {
            if (FinishedHours.Value > PlannedDayList.GetPlannedDay(plannedDayID).PlannedHours)
                FinishedHours.BackColor = Color.Green;
            else if (FinishedHours.Value < PlannedDayList.GetPlannedDay(plannedDayID).PlannedHours)
                FinishedHours.BackColor = Color.Red;
            else
                FinishedHours.BackColor = DefaultBackColor;
        }
    }
}
