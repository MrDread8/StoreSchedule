﻿using GrafikSklepowy.Class;
using System;
using System.Windows.Forms;

namespace GrafikSklepowy.Windows
{
    public partial class WorkingDayWindow : Form
    {
        public WorkingDayWindow()
        {
            InitializeComponent();
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            refreshTable(e.Start, e.End);
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Tag.ToString() == "PlanNew")
            {
                ManipulateWorkingDay mw = new ManipulateWorkingDay();
                this.Hide();
                mw.ShowDialog();
                WorkingDayList.AppendWorkingDay(mw.GetData);
                refreshTable(monthCalendar1.SelectionStart, monthCalendar1.SelectionEnd);
                this.Show();
            }
            else if (e.ClickedItem.Tag.ToString() == "EditPlan")
            {
                try
                {
                    if (workingDaysGrid.SelectedRows.Count == 0)
                        throw new Exception("Select Row!");
                    ManipulateWorkingDay mw = new ManipulateWorkingDay();
                    int id = (int)workingDaysGrid.SelectedRows[0].Tag;
                    mw.setDay(WorkingDayList.GetWorkingDay(id));
                    this.Hide();
                    mw.ShowDialog();
                    WorkingDayList.SetWorkingDay(id, mw.GetData);
                    refreshTable(monthCalendar1.SelectionStart, monthCalendar1.SelectionEnd);
                    this.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            else if (e.ClickedItem.Tag.ToString() == "RemovePlan")
            {
                WorkingDayList.RemoveEntry((int)workingDaysGrid.SelectedRows[0].Tag);
                refreshTable(monthCalendar1.SelectionStart, monthCalendar1.SelectionEnd);
            }
        }
        private void refreshTable(DateTime start, DateTime end)
        {
            workingDaysGrid.Rows.Clear();
            for (int i = 0; i < WorkingDayList.Count; i++)
            {
                if (WorkingDayList.GetWorkingDay(i).Date >= start && WorkingDayList.GetWorkingDay(i).Date <= end)
                {
                    int j = workingDaysGrid.Rows.Add(WorkingDayList.GetWorkingDay(i).Date, WorkingDayList.GetWorkingDay(i).ManHours);
                    workingDaysGrid.Rows[j].Tag = i;
                }
            }
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            refreshTable(e.Start, e.End);
        }
    }
}
