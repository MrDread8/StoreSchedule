﻿
namespace GrafikSklepowy
{
    partial class MainWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.WorkersButton = new System.Windows.Forms.ToolStripMenuItem();
            this.Plan = new System.Windows.Forms.ToolStripMenuItem();
            this.PlannedDays = new System.Windows.Forms.ToolStripMenuItem();
            this.Raports = new System.Windows.Forms.ToolStripMenuItem();
            this.raport3Grid = new System.Windows.Forms.DataGridView();
            this.WorkerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkerSurname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsHired = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Salary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.raport3Grid)).BeginInit();
            this.SuspendLayout();
            // 
            // Menu
            // 
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WorkersButton,
            this.Plan,
            this.PlannedDays,
            this.Raports});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(689, 24);
            this.Menu.TabIndex = 0;
            this.Menu.Text = "menuStrip1";
            // 
            // WorkersButton
            // 
            this.WorkersButton.Name = "WorkersButton";
            this.WorkersButton.Size = new System.Drawing.Size(62, 20);
            this.WorkersButton.Tag = "WorkersButton";
            this.WorkersButton.Text = "Workers";
            this.WorkersButton.Click += new System.EventHandler(this.WorkersButton_Click);
            // 
            // Plan
            // 
            this.Plan.Name = "Plan";
            this.Plan.Size = new System.Drawing.Size(92, 20);
            this.Plan.Tag = "WorkingButton";
            this.Plan.Text = "Working Days";
            this.Plan.Click += new System.EventHandler(this.Plan_Click);
            // 
            // PlannedDays
            // 
            this.PlannedDays.Name = "PlannedDays";
            this.PlannedDays.Size = new System.Drawing.Size(88, 20);
            this.PlannedDays.Tag = "PlannedDays";
            this.PlannedDays.Text = "Workers Plan";
            this.PlannedDays.Click += new System.EventHandler(this.PlannedDays_Click);
            // 
            // Raports
            // 
            this.Raports.Name = "Raports";
            this.Raports.Size = new System.Drawing.Size(59, 20);
            this.Raports.Tag = "RaportsButton";
            this.Raports.Text = "Raports";
            this.Raports.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Raports.Click += new System.EventHandler(this.Raports_Click);
            // 
            // raport3Grid
            // 
            this.raport3Grid.AllowUserToAddRows = false;
            this.raport3Grid.AllowUserToDeleteRows = false;
            this.raport3Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.raport3Grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WorkerName,
            this.WorkerSurname,
            this.IsHired,
            this.Salary});
            this.raport3Grid.Location = new System.Drawing.Point(12, 27);
            this.raport3Grid.Name = "raport3Grid";
            this.raport3Grid.RowTemplate.Height = 25;
            this.raport3Grid.Size = new System.Drawing.Size(659, 481);
            this.raport3Grid.TabIndex = 6;
            // 
            // WorkerName
            // 
            this.WorkerName.HeaderText = "Name";
            this.WorkerName.Name = "WorkerName";
            // 
            // WorkerSurname
            // 
            this.WorkerSurname.HeaderText = "Surname";
            this.WorkerSurname.Name = "WorkerSurname";
            // 
            // IsHired
            // 
            this.IsHired.HeaderText = "IsHired";
            this.IsHired.Name = "IsHired";
            // 
            // Salary
            // 
            this.Salary.HeaderText = "Salary";
            this.Salary.Name = "Salary";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 520);
            this.Controls.Add(this.raport3Grid);
            this.Controls.Add(this.Menu);
            this.MainMenuStrip = this.Menu;
            this.Name = "MainWindow";
            this.Text = "Okno Główne";
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.raport3Grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem WorkersButton;
        private System.Windows.Forms.ToolStripMenuItem Plan;
        private System.Windows.Forms.ToolStripMenuItem PlannedDays;
        private System.Windows.Forms.DataGridView raport3Grid;
        private System.Windows.Forms.ToolStripMenuItem Raports;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkerSurname;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsHired;
        private System.Windows.Forms.DataGridViewTextBoxColumn Salary;
    }
}

