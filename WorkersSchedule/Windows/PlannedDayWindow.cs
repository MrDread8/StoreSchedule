﻿using GrafikSklepowy.Class;
using System;
using System.Windows.Forms;

namespace GrafikSklepowy.Windows
{
    public partial class PlannedDayWindow : Form
    {
        public PlannedDayWindow()
        {
            InitializeComponent();
        }

        public void RefreshPlanGrid(DateTime start, DateTime end)
        {
            planGrid.Rows.Clear();
            for (int i = 0; i < PlannedDayList.Count; i++)
            {
                if (PlannedDayList.GetPlannedDay(i).Date >= start && PlannedDayList.GetPlannedDay(i).Date <= end)
                {
                    PlannedDay tmp = PlannedDayList.GetPlannedDay(i);
                    int j = planGrid.Rows.Add(tmp.Date, WorkerList.GetWorker(tmp.WorkerID).Name, WorkerList.GetWorker(tmp.WorkerID).Surname, tmp.PlannedHours, tmp.WorkHours);
                    planGrid.Rows[j].Tag = i;
                }
            }
        }

        private void calendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            RefreshPlanGrid(e.Start, e.End);
        }

        private void calendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            RefreshPlanGrid(e.Start, e.End);
        }

        private void Clearing_Click(object sender, EventArgs e)
        {
            if (planGrid.SelectedRows.Count == 0)
                throw new Exception("Select Row!");
            ClearingWindow cw = new ClearingWindow((int)planGrid.SelectedRows[0].Tag);
            cw.ShowDialog();
            RefreshPlanGrid(calendar.MinDate, calendar.MaxDate);
        }

        private void PlanNew_Click(object sender, EventArgs e)
        {
            ManipulatePlannedDay mp = new ManipulatePlannedDay();
            mp.ShowDialog();
            if (mp.getPlannedDay != null)
            {
                int tmp;
                if ((tmp = PlannedDayList.Find(mp.getPlannedDay.Date)) >= 0 && PlannedDayList.GetPlannedDay(tmp).WorkerID == mp.getPlannedDay.WorkerID)
                {
                    PlannedDayList.SetPlannedDay(tmp, mp.getPlannedDay);
                }
                else
                {
                    PlannedDayList.PlanNewDay(mp.getPlannedDay);
                }
                RefreshPlanGrid(calendar.MinDate, calendar.MaxDate);
            }
        }

        private void EditEntry_Click(object sender, EventArgs e)
        {
            if (planGrid.SelectedRows.Count == 0)
                throw new Exception("Select Row!");
            ManipulatePlannedDay mp = new ManipulatePlannedDay();
            int tmpID = (int)planGrid.SelectedRows[0].Tag;
            mp.SetData(PlannedDayList.GetPlannedDay(tmpID));
            mp.ShowDialog();
            if (mp.getPlannedDay != null)
            {
                PlannedDayList.SetPlannedDay(tmpID, mp.getPlannedDay);
                RefreshPlanGrid(calendar.MinDate, calendar.MaxDate);
            }
        }

        private void RemoveEntry_Click(object sender, EventArgs e)
        {
            if (planGrid.SelectedRows.Count == 0)
                throw new Exception("Select Row!");

            int tmpID = (int)planGrid.SelectedRows[0].Tag;
            PlannedDayList.RemovePlan(tmpID);
            RefreshPlanGrid(calendar.MinDate, calendar.MaxDate);
        }
    }
}
