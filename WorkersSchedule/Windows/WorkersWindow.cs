﻿using GrafikSklepowy.Class;
using System;
using System.Windows.Forms;
namespace GrafikSklepowy.Windows
{
    public partial class WorkersWindow : Form
    {
        public WorkersWindow()
        {
            InitializeComponent();
            refreshWorkerGrid();
        }
        private void refreshWorkerGrid()
        {
            workerGrid.Rows.Clear();
            for (int i = 0; i < WorkerList.Count; i++)
            {
                Worker w = WorkerList.GetWorker(i);
                if (w.IsHired)
                {
                    int r = workerGrid.Rows.Add(w.Name, w.Surname, w.Address, w.Birthday, w.TaxOffice, w.HireDate, w.HourlyRate, w.PhoneNumber);
                    workerGrid.Rows[r].Tag = i;
                }
            }
        }

        private void HireButton_Click(object sender, EventArgs e)
        {
            ManipulateWorker mw = new ManipulateWorker();
            mw.setID(WorkerList.Count);
            mw.ShowDialog();
            if (mw.Worker != null)
            {
                WorkerList.AppendWorker(mw.Worker);
            }
            refreshWorkerGrid();
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (workerGrid.SelectedRows.Count == 0)
                    throw new Exception("Worker not slected");
                ManipulateWorker mw = new ManipulateWorker();
                mw.setData(WorkerList.GetWorker((int)workerGrid.SelectedRows[0].Tag));
                mw.ShowDialog();
                if (mw.Worker != null)
                {
                    WorkerList.SetWorker((int)workerGrid.SelectedRows[0].Tag, mw.Worker);
                    refreshWorkerGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void FireButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (workerGrid.SelectedRows.Count == 0)
                    throw new Exception("Worker not selected");
                WorkerList.GetWorker((int)workerGrid.SelectedRows[0].Tag).Fire();
                workerGrid.Rows.Remove(workerGrid.SelectedRows[0]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
