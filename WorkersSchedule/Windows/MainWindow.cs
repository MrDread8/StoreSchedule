﻿using GrafikSklepowy.Class;
using GrafikSklepowy.Windows;
using System.Windows.Forms;
namespace GrafikSklepowy
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                loadAll();
            }
            catch
            {
                saveAll();
            }
        }
        private void refreshTable()
        {
            raport3Grid.Rows.Clear();
            for (int i = 0; i < WorkerList.Count; i++)
            {
                raport3Grid.Rows.Add(WorkerList.GetWorker(i).Name, WorkerList.GetWorker(i).Surname, WorkerList.GetWorker(i).IsHired ? "✓" : "🗶", WorkerList.GetWorker(i).Salary);
            }
        }
        private void saveAll()
        {
            WorkerList.Save();
            WorkingDayList.Save();
            PlannedDayList.Save();
            refreshTable();
        }
        private void loadAll()
        {
            WorkerList.Load();
            WorkingDayList.Load();
            PlannedDayList.Load();
            refreshTable();
        }

        private void WorkersButton_Click(object sender, System.EventArgs e)
        {
            WorkersWindow workerWidnow = new WorkersWindow();
            this.Hide();
            workerWidnow.ShowDialog();
            this.Show();
            saveAll();
        }

        private void Plan_Click(object sender, System.EventArgs e)
        {
            WorkingDayWindow wd = new WorkingDayWindow();
            this.Hide();
            wd.ShowDialog();
            this.Show();
            saveAll();
        }

        private void PlannedDays_Click(object sender, System.EventArgs e)
        {
            PlannedDayWindow pd = new PlannedDayWindow();
            this.Hide();
            pd.ShowDialog();
            this.Show();
            WorkerList.CalculateSalary();
            saveAll();
        }

        private void Raports_Click(object sender, System.EventArgs e)
        {
            SelectRaport sr = new();
            this.Hide();
            sr.ShowDialog();
            this.Show();
        }
    }
}
