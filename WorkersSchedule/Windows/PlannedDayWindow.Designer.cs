﻿
namespace GrafikSklepowy.Windows
{
    partial class PlannedDayWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.planGrid = new System.Windows.Forms.DataGridView();
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.PlanNew = new System.Windows.Forms.ToolStripMenuItem();
            this.EditEntry = new System.Windows.Forms.ToolStripMenuItem();
            this.RemoveEntry = new System.Windows.Forms.ToolStripMenuItem();
            this.Clearing = new System.Windows.Forms.ToolStripMenuItem();
            this.calendar = new System.Windows.Forms.MonthCalendar();
            this.WorkingDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerSurname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plannedWorkHours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finishedHours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.planGrid)).BeginInit();
            this.Menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // planGrid
            // 
            this.planGrid.AllowUserToAddRows = false;
            this.planGrid.AllowUserToDeleteRows = false;
            this.planGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.planGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WorkingDay,
            this.workerName,
            this.workerSurname,
            this.plannedWorkHours,
            this.finishedHours});
            this.planGrid.Location = new System.Drawing.Point(10, 27);
            this.planGrid.MultiSelect = false;
            this.planGrid.Name = "planGrid";
            this.planGrid.ReadOnly = true;
            this.planGrid.RowTemplate.Height = 25;
            this.planGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.planGrid.Size = new System.Drawing.Size(623, 448);
            this.planGrid.TabIndex = 0;
            // 
            // Menu
            // 
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PlanNew,
            this.EditEntry,
            this.RemoveEntry,
            this.Clearing});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(879, 24);
            this.Menu.TabIndex = 1;
            this.Menu.Text = "Menu";
            // 
            // PlanNew
            // 
            this.PlanNew.Name = "PlanNew";
            this.PlanNew.Size = new System.Drawing.Size(69, 20);
            this.PlanNew.Tag = "NewEntry";
            this.PlanNew.Text = "Plan New";
            this.PlanNew.Click += new System.EventHandler(this.PlanNew_Click);
            // 
            // EditEntry
            // 
            this.EditEntry.Name = "EditEntry";
            this.EditEntry.Size = new System.Drawing.Size(39, 20);
            this.EditEntry.Tag = "EditEntry";
            this.EditEntry.Text = "Edit";
            this.EditEntry.Click += new System.EventHandler(this.EditEntry_Click);
            // 
            // RemoveEntry
            // 
            this.RemoveEntry.Name = "RemoveEntry";
            this.RemoveEntry.Size = new System.Drawing.Size(52, 20);
            this.RemoveEntry.Tag = "RemoveEntry";
            this.RemoveEntry.Text = "Delete";
            this.RemoveEntry.Click += new System.EventHandler(this.RemoveEntry_Click);
            // 
            // Clearing
            // 
            this.Clearing.Name = "Clearing";
            this.Clearing.Size = new System.Drawing.Size(55, 20);
            this.Clearing.Tag = "ClearingEntry";
            this.Clearing.Text = "Rozlicz";
            this.Clearing.Click += new System.EventHandler(this.Clearing_Click);
            // 
            // calendar
            // 
            this.calendar.CalendarDimensions = new System.Drawing.Size(1, 3);
            this.calendar.Location = new System.Drawing.Point(645, 27);
            this.calendar.MaxSelectionCount = 30;
            this.calendar.Name = "calendar";
            this.calendar.TabIndex = 2;
            this.calendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calendar_DateChanged);
            this.calendar.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.calendar_DateSelected);
            // 
            // WorkingDay
            // 
            this.WorkingDay.HeaderText = "Working Day";
            this.WorkingDay.Name = "WorkingDay";
            this.WorkingDay.ReadOnly = true;
            // 
            // workerName
            // 
            this.workerName.HeaderText = "Worker Name";
            this.workerName.Name = "workerName";
            this.workerName.ReadOnly = true;
            // 
            // workerSurname
            // 
            this.workerSurname.HeaderText = "Worker Surname";
            this.workerSurname.Name = "workerSurname";
            this.workerSurname.ReadOnly = true;
            // 
            // plannedWorkHours
            // 
            this.plannedWorkHours.HeaderText = "Planned Hours";
            this.plannedWorkHours.Name = "plannedWorkHours";
            this.plannedWorkHours.ReadOnly = true;
            // 
            // finishedHours
            // 
            this.finishedHours.HeaderText = "Done";
            this.finishedHours.Name = "finishedHours";
            this.finishedHours.ReadOnly = true;
            // 
            // PlannedDayWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 490);
            this.Controls.Add(this.calendar);
            this.Controls.Add(this.planGrid);
            this.Controls.Add(this.Menu);
            this.MainMenuStrip = this.Menu;
            this.Name = "PlannedDayWindow";
            this.Text = "Planned Days";
            ((System.ComponentModel.ISupportInitialize)(this.planGrid)).EndInit();
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem PlanNew;
        private System.Windows.Forms.ToolStripMenuItem EditEntry;
        private System.Windows.Forms.ToolStripMenuItem RemoveEntry;
        private System.Windows.Forms.MonthCalendar calendar;
        private System.Windows.Forms.DataGridView planGrid;
        private System.Windows.Forms.ToolStripMenuItem Clearing;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkingDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerSurname;
        private System.Windows.Forms.DataGridViewTextBoxColumn plannedWorkHours;
        private System.Windows.Forms.DataGridViewTextBoxColumn finishedHours;
    }
}