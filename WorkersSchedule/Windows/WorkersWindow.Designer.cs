﻿
namespace GrafikSklepowy.Windows
{
    partial class WorkersWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.workerGrid = new System.Windows.Forms.DataGridView();
            this.DataName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Surname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Birthday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxOffice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HireDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HourlyRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.HireButton = new System.Windows.Forms.ToolStripMenuItem();
            this.EditButton = new System.Windows.Forms.ToolStripMenuItem();
            this.FireButton = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.workerGrid)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // workerGrid
            // 
            this.workerGrid.AllowUserToAddRows = false;
            this.workerGrid.AllowUserToDeleteRows = false;
            this.workerGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.workerGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DataName,
            this.Surname,
            this.Address,
            this.Birthday,
            this.TaxOffice,
            this.HireDate,
            this.HourlyRate,
            this.PhoneNumber});
            this.workerGrid.Location = new System.Drawing.Point(12, 33);
            this.workerGrid.MultiSelect = false;
            this.workerGrid.Name = "workerGrid";
            this.workerGrid.ReadOnly = true;
            this.workerGrid.RowTemplate.Height = 25;
            this.workerGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.workerGrid.Size = new System.Drawing.Size(846, 405);
            this.workerGrid.TabIndex = 1;
            // 
            // DataName
            // 
            this.DataName.HeaderText = "Imię";
            this.DataName.Name = "DataName";
            this.DataName.ReadOnly = true;
            // 
            // Surname
            // 
            this.Surname.HeaderText = "Nazwisko";
            this.Surname.Name = "Surname";
            this.Surname.ReadOnly = true;
            // 
            // Address
            // 
            this.Address.HeaderText = "Adres";
            this.Address.Name = "Address";
            this.Address.ReadOnly = true;
            // 
            // Birthday
            // 
            this.Birthday.HeaderText = "Data Urodzenia";
            this.Birthday.Name = "Birthday";
            this.Birthday.ReadOnly = true;
            // 
            // TaxOffice
            // 
            this.TaxOffice.HeaderText = "Podatek";
            this.TaxOffice.Name = "TaxOffice";
            this.TaxOffice.ReadOnly = true;
            // 
            // HireDate
            // 
            this.HireDate.HeaderText = "Data Zatrudnienia";
            this.HireDate.Name = "HireDate";
            this.HireDate.ReadOnly = true;
            // 
            // HourlyRate
            // 
            this.HourlyRate.HeaderText = "Stawka Godzinowa";
            this.HourlyRate.Name = "HourlyRate";
            this.HourlyRate.ReadOnly = true;
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.HeaderText = "Numer Tel.";
            this.PhoneNumber.Name = "PhoneNumber";
            this.PhoneNumber.ReadOnly = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HireButton,
            this.EditButton,
            this.FireButton});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(872, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // HireButton
            // 
            this.HireButton.Name = "HireButton";
            this.HireButton.Size = new System.Drawing.Size(41, 20);
            this.HireButton.Tag = "HireButton";
            this.HireButton.Text = "Hire";
            this.HireButton.Click += new System.EventHandler(this.HireButton_Click);
            // 
            // EditButton
            // 
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(39, 20);
            this.EditButton.Tag = "EditButton";
            this.EditButton.Text = "Edit";
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // FireButton
            // 
            this.FireButton.Name = "FireButton";
            this.FireButton.Size = new System.Drawing.Size(38, 20);
            this.FireButton.Tag = "FireButton";
            this.FireButton.Text = "Fire";
            this.FireButton.Click += new System.EventHandler(this.FireButton_Click);
            // 
            // WorkersWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 450);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.workerGrid);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "WorkersWindow";
            this.Text = "Pracownicy";
            ((System.ComponentModel.ISupportInitialize)(this.workerGrid)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView workerGrid;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem HireButton;
        private System.Windows.Forms.ToolStripMenuItem EditButton;
        private System.Windows.Forms.ToolStripMenuItem FireButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Surname;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn Birthday;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxOffice;
        private System.Windows.Forms.DataGridViewTextBoxColumn HireDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn HourlyRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNumber;
    }
}